"use strict";

for (var x = 1; x <= 82; x++) {
    $.ajax("https://swapi.dev/api/people/" + x + "/", {
        type: "GET"
    }).done(function (res) {
        console.log(res);

        //gathering all of the information needed from the res (response) variable
        var name = res.name;
        var height = res.height;
        var mass = res.mass;
        var birth_year = res.birth_year;
        var gender = res.gender;
        // var home = res.homeworld;
        // var films = res.films;


        // formatting the display
        var character_row = `
                <div class="col-6">
                    <div class="card">
                    <div class="card-header">
                    ${name}
                    </div>
                    <ul class="list-group list-group-flush text-light">
                        <li class="list-group-item">Height: ${height}</li>
                        <li class="list-group-item">Mass: ${mass}</li>
                        <li class="list-group-item">Birth Year: ${birth_year}</li>
                        <li class="list-group-item">Gender: ${gender}</li>
                    </ul>
                    </div>
                </div>
                `;

        //appending the variable 'character_row' to #insertCharacters
        $("#insertCharacters").append(character_row);

    });
}
